﻿"Blisslight" theme updated to use new Recalbox 9.2+ syntax - 2024-02-09 by Pit64.

If you have some assets to add for the missing systems or any configuration for crt, you can still do a merge request and it will be reviewed.

## Original readme file

Theme "Blisslight" for EmulationStation (http://www.emulationstation.org/) and Recalbox (http://www.recalbox.com/)

Based on the theme "simplelight"  by:
(c) Nils Bonenberger - nilsbyte@nilsbyte.de - http://www.nilsbyte.de/
(c) Recalbox - http://www.recalbox.com/


Changelog
=========

- 28-6-2017 Multiple changes (By RetroWilly)
  - Cleaned up old unused files
  - Minor logo changes

- 26-1-2017 (By RetroWilly)
  -BackgroundImages overhaul
  -System Icon changes
  -Changed different system music
  -Artwork fix

- 13-09-2015 (by recalbox community)
  - Added Cave Story theme

- 07-01-2015 (by recalbox community)
  - Changed backgrounds resolution to 480p for save vram and load all the systemes in Pi1 512ram
  - Added favoris integration
  - Added background music to all themes
  - Updated theme with simple 1.4
  - created missing themes
  - Cleaned no supported themes and graphic elements

- 02-28-2014 (by digitalLumberjack)
  - Added backgrounds to detailled view
  - Added background music to all themes
  - created missing themes

- 06-16-2014
  1.0 Initial version


License
=======

See LICENSE.